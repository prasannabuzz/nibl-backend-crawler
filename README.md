# **Nepal Investment Bank BackEnd**

## **Pre-requisites**
- Task Scheduler
- Any suitable GIT client
- Notepad/Notepad++

*This guide is for deploying and updating the app in local environment*

## **Database Configuration in 'NIBL.Service.WebScraper.exe.config' and 'NIBL.Service.WebScraper.vshost.exe.config'**

1.  Navigate to the folder where app to be deployed.
2.  Navigate to the file 'NIBL.Service.WebScraper.exe.config' and 'NIBL.Service.WebScraper.vshost.exe.config' within the root path and edit in notepad.
3.  Find the key name NIBLDbContext between the tags <connectionstrings></connectionstrings> and edit the following with production database information.

    - server : server name of database instance (domain or ip address)
	- port : port number
    - userid : username
    - password : password
    - database : database name

	For example:

	- server = 127.0.0.1
	- port = 3306
	- userid = nibl_user
	- password = nibl_password
	- database = db_nibl 

4. Save the file.


## **Deploy**
1.	Clone the repository with any suitable GIT client into the folder where app is to be deployed.
2.	Alternatively, you can download the repository as a ZIP file.
3.	Create task schedule in windows using 'Task Scheduler' to run the application 'NIBL.Service.WebScraper.exe'.